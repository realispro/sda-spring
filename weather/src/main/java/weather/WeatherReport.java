package weather;

import java.time.LocalDateTime;

public class WeatherReport {

    private final LocalDateTime timestamp;

    private float temperature;
    private int pressure;

    public WeatherReport(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public WeatherReport withTemperature(float temperature){
        this.temperature = temperature;
        return this;
    }

    public WeatherReport withPressure(int pressure){
        this.pressure = pressure;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public float getTemperature() {
        return temperature;
    }

    public int getPressure() {
        return pressure;
    }
}
