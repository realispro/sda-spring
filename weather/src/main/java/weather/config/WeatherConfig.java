package weather.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.List;

@Configuration
@PropertySource("classpath:weather.properties")
public class WeatherConfig {

    private Environment environment;

    public WeatherConfig(Environment environment) {
        this.environment = environment;
    }

    @Bean("services")
    List<String> services(@Value("${meteo.service}") String customService){
        return List.of(
                "meteo.pl",
                "weather.com",
                customService
        );
    }

}
