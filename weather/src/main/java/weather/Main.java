package weather;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import weather.sensors.CelciusTemperatureSensor;
import weather.sensors.PascalPressureSensor;

import java.time.LocalDateTime;

@ComponentScan("weather")
public class Main {

    public static void main(String[] args) {
        System.out.println("Let's check current weather!");

        ApplicationContext context = new AnnotationConfigApplicationContext(Main.class);
                //new ClassPathXmlApplicationContext("classpath:context.xml");

        ReportCollector collector = context.getBean(ReportCollector.class);
        ReportCollector collector2 = context.getBean(ReportCollector.class);
                /*new SimpleReportCollector();
        ((SimpleReportCollector)collector).setPressureSensor(new PascalPressureSensor());
        ((SimpleReportCollector)collector).setTemperatureSensor(new CelciusTemperatureSensor());*/

        WeatherReport report = collector.collect(LocalDateTime.now().plusHours(1));
        System.out.println("Current weather report at [" + report.getTimestamp() + "]" );
        System.out.println("temperature [" + report.getTemperature() + "]");
        System.out.println("pressure [" + report.getPressure() + "]");

        System.out.println("done.");
    }
}
