package weather;

public interface TemperatureSensor {

    float getTemperature();
}
