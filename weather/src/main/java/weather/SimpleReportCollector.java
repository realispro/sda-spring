package weather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
@Lazy
public class SimpleReportCollector implements ReportCollector {

    private TemperatureSensor temperatureSensor;
    private PressureSensor pressureSensor;

    private List<String> services = new ArrayList<>();

    /*public SimpleReportCollector() {
        System.out.println("creating collector instance");
    }*/

    //@Autowired
    public SimpleReportCollector(
            @Qualifier("celcius") TemperatureSensor temperatureSensor,
            PressureSensor pressureSensor) {
        System.out.println("creating collector instance with params");
        this.temperatureSensor = temperatureSensor;
        this.pressureSensor = pressureSensor;
    }

    @PostConstruct
    public void init(){
        System.out.println("post construct report collector");
    }

    @Override
    public WeatherReport collect(LocalDateTime dateTime){

        System.out.println("collecting weather data at "
                + dateTime.format(DateTimeFormatter.ISO_DATE_TIME));
        System.out.println("collector registered at services " + services);

        return new WeatherReport(dateTime)
                .withTemperature(temperatureSensor.getTemperature())
                .withPressure(pressureSensor.getPressure());
    }

    public TemperatureSensor getTemperatureSensor() {
        return temperatureSensor;
    }


    public void setTemperatureSensor(TemperatureSensor temperatureSensor) {
        this.temperatureSensor = temperatureSensor;
    }

    public PressureSensor getPressureSensor() {
        return pressureSensor;
    }


    public void setPressureSensor(PressureSensor pressureSensor) {
        this.pressureSensor = pressureSensor;
    }

    public List<String> getServices() {
        return services;
    }

    @Autowired
    public void setServices(List<String> services) {
        this.services = services;
    }
}
