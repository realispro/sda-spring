package weather;

public interface PressureSensor {

    int getPressure();
}
