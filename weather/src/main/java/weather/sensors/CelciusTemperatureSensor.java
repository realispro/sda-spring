package weather.sensors;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import weather.TemperatureSensor;

import java.util.Random;

@Component("temperatureSensor")
@Qualifier("celcius")
public class CelciusTemperatureSensor implements TemperatureSensor {
    @Override
    public float getTemperature() {
        System.out.println("generating random temp in celcius");
        return new Random().nextFloat();
    }
}
