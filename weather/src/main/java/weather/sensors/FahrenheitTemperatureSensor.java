package weather.sensors;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import weather.TemperatureSensor;

import java.util.Random;

@Component
@Primary
public class FahrenheitTemperatureSensor implements TemperatureSensor {
    @Override
    public float getTemperature() {
        System.out.println("generating random temp in farhenheit");
        return new Random(System.currentTimeMillis()).nextFloat();
    }
}
