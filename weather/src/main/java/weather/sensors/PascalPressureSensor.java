package weather.sensors;


import org.springframework.stereotype.Component;
import weather.PressureSensor;

import java.util.Random;

@Component
public class PascalPressureSensor implements PressureSensor {
    @Override
    public int getPressure() {
        return new Random(System.currentTimeMillis()).nextInt(100);
    }
}
