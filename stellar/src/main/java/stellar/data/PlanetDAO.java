package stellar.data;


import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;

public interface PlanetDAO extends JpaRepository<Planet, Integer> {

    // List<Planet> findAll();

    default List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return findAllBySystem(system);
    }
    List<Planet> findAllBySystem(PlanetarySystem system);
    default List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return findAllBySystemAndNameContaining(system, like);
    }
    List<Planet> findAllBySystemAndNameContaining(PlanetarySystem system, String like);
//    Optional<Planet> findById(int id);
//
//    Planet save(Planet p);
}
