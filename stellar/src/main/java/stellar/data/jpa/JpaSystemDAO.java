/*
package stellar.data.jpa;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import stellar.data.SystemDAO;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Repository
@Primary
@Slf4j
public class JpaSystemDAO implements SystemDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<PlanetarySystem> findAll() {
        // JPQL -> HQL -> SQL
        return em.createQuery("select s from PlanetarySystem s").getResultList();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return em.createQuery("select s from PlanetarySystem s where s.name like :likeName")
                .setParameter("likeName", "%" + like + "%")
                .getResultList();
    }

    @Override
    public Optional<PlanetarySystem> findById(int id) {
        return Optional.ofNullable(em.find(PlanetarySystem.class, id));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public PlanetarySystem save(PlanetarySystem system) {
        em.persist(system);
        return system;
    }
}
*/
