/*
package stellar.data.jpa;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.data.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Repository
@Primary
@Slf4j
public class JpaPlanetDAO implements PlanetDAO {

    @PersistenceContext
    private EntityManager em;
    @Override
    public List<Planet> findAll() {
        return em.createQuery("select p from Planet p").getResultList();
    }
    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return em.createQuery("select p from Planet p where p.system like :system")
                .setParameter("system", system)
                .getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return em.createQuery("select p from Planet p where p.system like :system and p.name like :like")
                .setParameter("system", system)
                .setParameter("like", "%" + like + "%")
                .getResultList();
    }

    @Override
    public Optional<Planet> findById(int id) {
        return Optional.ofNullable(em.find(Planet.class, id));
    }
    @Override
    public Planet save(Planet p) {
        em.persist(p);
        return p;
    }
}
*/
