package stellar.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;

public interface SystemDAO extends JpaRepository<PlanetarySystem, Integer> {

    //List<PlanetarySystem> findAll();

    default List<PlanetarySystem> getPlanetarySystemsByName(String like){
        return findAllByNameContaining(like);
    };

    @Query("select s from PlanetarySystem s where s.name like :name")
    List<PlanetarySystem> findAllByNameContaining(String name);


    //Optional<PlanetarySystem> findById(int id);

    //PlanetarySystem save(PlanetarySystem system);

}
