/*
package stellar.data.mem;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import stellar.data.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class InMemorySystemDAO implements SystemDAO {

    @Override
    public List<PlanetarySystem> findAll() {
        return InMemory.systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return InMemory.systems.stream().filter(s->s.getName().toLowerCase().contains(like.toLowerCase())).collect(Collectors.toList());
    }

    @Override
    public Optional<PlanetarySystem> findById(int id) {
        return InMemory.systems.stream().filter(s->s.getId()==id).findFirst();
    }

    @Override
    public PlanetarySystem save(PlanetarySystem system) {
        int idMax = InMemory.systems.stream().sorted((s1, s2)->s2.getId()-s1.getId()).findFirst().get().getId();

        system.setId(++idMax);
        InMemory.systems.add(system);
        return system;
    }
}*/
