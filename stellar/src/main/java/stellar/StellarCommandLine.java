package stellar;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

//@Component
@RequiredArgsConstructor
@Slf4j
public class StellarCommandLine implements CommandLineRunner {

    private final StellarService service;

    @Override
    public void run(String... args) throws Exception {
        log.info("starting command line code...");
        PlanetarySystem system = service.getSystemById(1);
        log.info("Planets of system {}:", system.getName());
        List<Planet> planets = service.getPlanets(system);
        planets.forEach(p->log.info(p.toString()));
    }
}
