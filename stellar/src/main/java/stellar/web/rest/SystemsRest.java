package stellar.web.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class SystemsRest {

    private final StellarService service;
    private final SystemValidator validator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }

    @GetMapping("/systems") // /systems?name=Solar
    List<PlanetarySystem> getSystems(
            @RequestParam(value = "name", required = false) String name,
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @RequestHeader Map<String, String> headers
            ){
        log.info("fetching systems. Name predicate: {}", name);
        log.info("user agent: {}", userAgent);
        log.info("headers: {}", headers);

        List<PlanetarySystem> systems = name==null
                ? service.getSystems()
                : service.getSystemsByName(name);
        return systems;
    }

    @GetMapping("/systems/{id}") // /systems/1
    ResponseEntity<PlanetarySystem> getSystem(@PathVariable("id") int id){
        log.info("fetching system {}", id);

        PlanetarySystem system = service.getSystemById(id);
        if(system==null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(system);
        }
    }

    @PostMapping("/systems")
    ResponseEntity<?> addSystem(@RequestBody @Validated PlanetarySystem system, Errors errors){
        log.info("about to add new system {}", system);

        if(errors.hasErrors()){

            StringBuilder sb = new StringBuilder("Validation failed:\n");
            errors.getAllErrors().forEach(e->sb.append(e.getCode()).append("\n"));

            return ResponseEntity.badRequest().body(sb.toString());
        }

        system = service.addPlanetarySystem(system);

        return ResponseEntity.status(HttpStatus.CREATED).body(system);
    }

}
