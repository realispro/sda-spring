package stellar.web.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class PlanetsRest {

    private final StellarService service;

    @GetMapping("/systems/{id}/planets")
    ResponseEntity<List<Planet>> getPlanets(@PathVariable("id") int id) {
        log.info("fetching plantes from system: {}", id);
        PlanetarySystem system = service.getSystemById(id);
        if (system == null) {
            return ResponseEntity.notFound().build();
                    //ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        List<Planet> planets = service.getPlanets(system);
        return ResponseEntity.ok(planets);
                //ResponseEntity.status(HttpStatus.OK).body(planets);
    }

    @GetMapping("/planets/{id}")
    ResponseEntity<Planet> getPlanet(@PathVariable("id") int id) {
        log.info("fetching planet by is: {}", id);
        Planet planet = service.getPlanetById(id);
        if (planet == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(planet);
    }
}