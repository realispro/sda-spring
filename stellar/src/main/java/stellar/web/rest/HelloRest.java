package stellar.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloRest {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    String hello(){
        log.info("hello request received");
        return "Hey Universe!";
    }

}
