package stellar.web.mvc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(basePackages = "stellar.web.mvc")
@Slf4j
public class StellarAdvice {

    @ExceptionHandler(IllegalArgumentException.class)
    ModelAndView handleException(IllegalArgumentException e){
        log.error("illegal argument exception handled", e);
        ModelAndView modelAndView = new ModelAndView("errorView");
        modelAndView.addObject("message", "Illegal argument exception: " + e.getMessage());
        return modelAndView;
    }

    @ExceptionHandler(Exception.class)
    ModelAndView handleException(Exception e){
        log.error("illegal argument exception handled", e);
        ModelAndView modelAndView = new ModelAndView("errorView");
        modelAndView.addObject("message", "General exception: " + e.getMessage());
        return modelAndView;
    }
}
