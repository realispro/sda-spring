package stellar.web.mvc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

@Component
@Slf4j
public class TimeInterceptor implements HandlerInterceptor {

    @Value("${stellar.opening:9}")
    private int opening;
    @Value("${stellar.closing:16}")
    private int closing;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        int hour = LocalTime.now().getHour();
        if(hour<opening || hour>=closing){
            log.warn("service is closed");
            response.sendRedirect("https://img.freepik.com/free-psd/3d-rendering-coffee-shop-icon_23-2149879003.jpg?w=900&t=st=1682669397~exp=1682669997~hmac=f8b1e1fdea9959ddd1122a199f37f28052bb4bdf078d89be266da368a7f50c0c");
            return false;
        }

        return true;
    }
}
