package stellar.web.mvc;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@Slf4j
public class PlanetsController {

    private final StellarService service;

    @GetMapping(value = "/planets")
    String getPlanets(Model model, @RequestParam(value = "systemId", required = false) int systemId) {
        log.info("fetch planet of system {}", systemId);

        if(systemId==303){
            throw new RuntimeException("303 not allowed");
        }

        PlanetarySystem system = service.getSystemById(systemId);
        List<Planet> planets = service.getPlanets(system);

        model.addAttribute("planets", planets);
        model.addAttribute("system", system);

        return "planets";
    }


}