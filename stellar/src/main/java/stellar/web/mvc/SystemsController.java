package stellar.web.mvc;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@Slf4j
public class SystemsController {

    private final StellarService service;

    @GetMapping("/systems")
    String getSystems(Model model, @RequestParam(value = "phrase", required = false) String phrase){
        log.info("about to provide systems");

        if("foo".equals(phrase)){
            throw new IllegalArgumentException("foo not allowed");
        }

        List<PlanetarySystem> systems = phrase==null
                ? service.getSystems()
                : service.getSystemsByName(phrase);

        model.addAttribute("systems", systems); // attribute name

        return "systems"; // view name
    }

}
