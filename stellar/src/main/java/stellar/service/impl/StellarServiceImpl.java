package stellar.service.impl;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import stellar.data.PlanetDAO;
import stellar.data.SystemDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
@RequiredArgsConstructor
public class StellarServiceImpl implements StellarService {

    private static Logger logger = Logger.getLogger(StellarServiceImpl.class.getName());

    private final SystemDAO systemDAO;
    private final PlanetDAO planetDAO;
    //private final PlatformTransactionManager transactionManager;

    @Override
    public List<PlanetarySystem> getSystems() {
        logger.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.findAll();
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        logger.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems = systemDAO.getPlanetarySystemsByName(like);
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        logger.info("fetching planetary system by id " + id);

        Optional<PlanetarySystem> system = systemDAO.findById(id);
        //logger.info("found: " + system);
        return system.orElse(null);
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        logger.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        logger.info("found:" + planets.size());
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system, like);
    }


    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.findById(id).orElse(null);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        p.setSystem(s);
        return planetDAO.save(p);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {

        /*TransactionStatus ts = transactionManager.getTransaction(new DefaultTransactionDefinition());
        try{*/
        s = systemDAO.save(s);
          /*  transactionManager.commit(ts);
        }catch (RuntimeException e){
            transactionManager.rollback(ts);
            throw e;
        }*/

        return s;
    }

}
