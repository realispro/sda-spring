package stellar.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

@ComponentScan("stellar")
public class StellarServiceMain {

    public static void main(String[] args) {
        System.out.println("Let's count planets!");


        ApplicationContext context = new AnnotationConfigApplicationContext(StellarServiceMain.class);

        StellarService service = context.getBean(StellarService.class);
                /*new StellarServiceImpl();
        ((StellarServiceImpl)service).setSystemDAO(new InMemorySystemDAO());
        ((StellarServiceImpl)service).setPlanetDAO(new InMemoryPlanetDAO());*/
        PlanetarySystem system = service.getSystemById(1);
        List<Planet> planets = service.getPlanets(system);
        System.out.printf("Planets of %s system:", system.getName());
        planets.forEach(System.out::println);

        System.out.println("done.");
    }
}
