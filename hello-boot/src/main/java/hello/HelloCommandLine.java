package hello;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HelloCommandLine implements CommandLineRunner {

    private String greetings;

    public HelloCommandLine(String greetings) {
        this.greetings = greetings;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("greetings : " + greetings );
    }
}
