package hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelloConfig {

    @Bean
    String greetings(@Value("${greetings.name}") String name){
        return "Hey " + name + "!";
    }
}
